import cv2 as cv

img = cv.imread('data/face1.jpg')
img_resized= cv.resize(img,(img.shape[1]//2,img.shape[0]//2))
cv.imshow('Person',img_resized)

gray =cv.cvtColor(img_resized,cv.COLOR_BGR2GRAY)
cv.imshow('gray Person',gray)

haar_cascade = cv.CascadeClassifier('data/haarcascade_frontalface_default.xml')

face_rect = haar_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=3)

print(f'Number of face found = {len(face_rect)}')

for (x,y,w,h) in face_rect:
    cv.rectangle(img_resized,(x,y),(x+w,y+h),(0,255,0),thickness=2)

cv.imshow('gray Person',img_resized)

cv.waitKey(0)